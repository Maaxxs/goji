# Goji

Convienently browse books and papers

## Build and Install

### Dependencies

- gtk+-3.0

To Build run

```
meson build --prefix=/usr
cd build
ninja
```

To install run

 ```
sudo ninja install
```

Get Debug Messages by setting `G_MESSAGES_DEBUG=all`.

```
G_MESSAGES_DEBUG=all ./com.gitlab.maaxxs.goji
```
